#include "../inc/inventory.h"

int inventory::reserve_upc() {
    srand(42);
    auto random_upc_location = int(rand()%upc_generator.size());
    auto a = upc_generator.begin();
    for(auto a = upc_generator.begin(); --random_upc_location != 0; a++);
    while(!a->second && a != upc_generator.end()) a++;
    a->second = false;
    return a->first;
}

void inventory::release_upc(int input_upc) {
    auto val =  upc_generator.find(input_upc);
    if(val->first == input_upc && val->second == false){
        val->second = true;
        return;
    }
    else throw "UPC not valid";
}

bool inventory::valid_upc(int input_upc) {
    if(input_upc == 0) return false;
    auto val = inventory::upc_generator.find(input_upc);
    return val->first == input_upc;
}

void inventory::initialize_upc() {
    srand(17); // Initial seed
    for(int i = 1; i< 1000; i++) {
        int unique_upc = 1000000 + rand() % 9000000;
        inventory::upc_generator.insert(std::pair<int, bool>(unique_upc, true));
    }
}

inventory::inventory() {
    head = nullptr;
    tail = nullptr;
    initialize_upc();
}

inventory::~inventory() {
    inventory_node *temp;
    while(head != nullptr){
        temp = head;
        head = head->next;
        delete temp;
    }
}

void inventory::add_sku(std::string new_name, int initial_price, int initial_inventory, int initial_date) {
    int upc = reserve_upc();
    if(isEmpty()) {
        head=tail=new inventory_node(upc,new_name,initial_inventory,initial_price,initial_date);
        return;
    }

    tail->next=new inventory_node(upc,new_name,initial_inventory,initial_price,initial_date);
    tail = tail->next;

}

void inventory::remove_sku(int input_upc) {
    if(head == nullptr){
        return;
    }
    inventory_node *temp = head;
    inventory_node *temp1 = temp;
    if(head->upc == input_upc){
        head = head->next;
        return;
    }else{
        while(temp != nullptr){
            if(temp->upc == input_upc){
                if(temp == tail){
                    tail = temp1;
                    tail->next = nullptr;
                    return;
                }else{
                    tail->next = temp->next;
                    return;
                }
            }
            temp1 = temp;
            temp = temp->next;
        }
    }
}

std::vector<int> inventory::get_upc(std::string input_name) {
    std::vector<int> temp;
    inventory_node *temp1 = head;
    while(temp1 != nullptr){
        if(temp1->name == input_name){
            temp.push_back(temp1->upc);
        }
        temp1 = temp1->next;
    }
    return temp;
}

int inventory::get_price(int input_upc) {
    inventory_node *temp = head;
    while (temp != nullptr){
        if(temp->upc == input_upc){
            return temp->price.top().value;
        }
        temp = temp->next;
    }
}

int inventory::get_inventory(int input_upc) {
    inventory_node *temp = head;
    while(temp->next != nullptr){
        if(temp->upc == input_upc){
            return temp->inventory_count;
        }else{
            temp = temp->next;
        }
    }
}

std::string inventory::get_name(int input_upc) {
    inventory_node *temp = head;
    while(temp->next != nullptr){
        if(temp->upc == input_upc){
            return temp->name;
        }else{
            temp = temp->next;
        }
    }throw("Does not Exist");
}

void inventory::adjust_price(int input_upc, int new_price, int new_date) {
    inventory_node *temp = head;
    value_date temp1;
    temp1.value = new_price;
    temp1.value = new_date;
    while(temp != nullptr){
        if(temp->upc == input_upc){
            temp->price.push(temp1);
        }
        temp = temp->next;
    }
}

void inventory::adjust_inventory(int input_upc, int new_inventory) {
    inventory_node *temp = head;
    while(temp != nullptr){
        if(temp->upc == input_upc){
            temp->inventory_count = new_inventory;
        }
        temp = temp->next;
    }
}

inventory_node *inventory::get_head() {
    return head;
}

bool inventory::isEmpty() const {
    if((head && !tail) || (!head && tail))throw std::logic_error ("Error head and tail are not both null"); // this should never happen internal error
    return !head;
}