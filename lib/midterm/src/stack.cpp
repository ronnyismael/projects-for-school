#include "../inc/stack.h"

stack::stack() {
    linked_list_m temp;
    storage_structure = temp;
}

stack::~stack() {
    while(!storage_structure.isEmpty()){
        storage_structure.remove(0);
    }
}

void stack::pop() {
    storage_structure.remove(0);
}

void stack::push(struct value_date input) {
    storage_structure.insert(input, 0);
}

const struct value_date stack::top() const {
    if(storage_structure.isEmpty()){
        value_date temp;
        return temp;
    }
    return storage_structure.get_value_at(0);
}

bool stack::empty() {
    if(!storage_structure.isEmpty()){
        return false;
    }else{
        return true;
    }
}

stack &stack::operator=(const stack &RHS) {
    if (this == &RHS){
        return *this;
    }else{
        this->storage_structure = RHS.storage_structure;
        return *this;
    }
}

