#include "../inc/tree.h"
#include <iostream>
#include <algorithm>

namespace lab7 {
    void clear(node *to_clear);

    // Construct an empty tree
    tree::tree() {
        root = nullptr;
        tree_size = 0;
    }

    // Copy constructor
    tree::tree(const tree &copy) {

    }

    // Deconstruct tree
    tree::~tree() {
        clear(root);
    }

    void tree::insert_helper(node *&root, int value) {
        // insert value into tree recursively using root and update frequency
        if (root == nullptr) {
            root = new node(value);
            tree_size++;
        } else if (value < root->data) {
            insert_helper(root->left, value);
        } else if (value > root->data) {
            insert_helper(root->right, value);
        } else {
            root->frequency++;
        }


    }

    // Insert
    void tree::insert(int value) {
        insert_helper(root, value);
    }

    // Remove key return true if the key is deleted, and false if it isn't in the tree. decrease frequency if key is found. remove node if frequency is 1.
    bool tree::remove(int key) {
        // remove key from tree iteratively using root and update frequency
        if (root == nullptr) {
            return false;
        }
        // if root is key, remove root
        if (root->data == key) {
            if (root->frequency > 1) {
                root->frequency--;
                return true;
            } else {
                node *temp = root;
                root = root->right;
                delete temp;
                tree_size--;
                return true;
            }
        }
        node *current = root;
        node *parent = nullptr;
        while (current != nullptr) {
            if (current->data == key) {
                if (current->frequency > 1) {
                    current->frequency--;
                    return true;
                } else if (current->frequency == 1) {
                    if (current->left == nullptr && current->right == nullptr) {
                        if (parent->left == current) {
                            parent->left = nullptr;
                        } else {
                            parent->right = nullptr;
                        }
                        delete current;
                        tree_size--;
                        return true;
                    } else if (current->left == nullptr) {
                        if (parent->left == current) {
                            parent->left = current->right;
                        } else {
                            parent->right = current->right;
                        }
                        delete current;
                        tree_size--;
                        return true;
                    } else if (current->right == nullptr) {
                        if (parent->left == current) {
                            parent->left = current->left;
                        } else {
                            parent->right = current->left;
                        }
                        delete current;
                        tree_size--;
                        return true;
                    } else {
                        node *temp = current->right;
                        while (temp->left != nullptr) {
                            temp = temp->left;
                        }
                        temp->left = current->left;
                        if (parent->left == current) {
                            parent->left = current->right;
                        } else {
                            parent->right = current->right;
                        }
                        delete current;
                        tree_size--;
                        return true;
                    }
                }
            } else if (current->data > key) {
                parent = current;
                current = current->left;
            } else {
                parent = current;
                current = current->right;
            }
        }

    }

    int tree::level_helper(node *root, int key, int level) {
        // return the level of key in the tree
        if (root == nullptr) {
            return -1;
        }
        if (root->data == key) {
            return level;
        }
        int left = level_helper(root->left, key, level + 1);
        if (left != -1) {
            return left;
        }
        int right = level_helper(root->right, key, level + 1);
        if (right != -1) {
            return right;
        }
        return -1;
    }

    // What level is key on?
    int tree::level(int key) {
        return level_helper(root, key, 0);

    }

    std::string tree::path_to_helper(node *root, int key, std::string &path) {
        // return the path values from root to key with a space in between
        if (root == nullptr) {
            return "";
        } else if (root->data == key) {
            path += std::to_string(root->data);
            return path;
        } else if (key < root->data) {
            path += std::to_string(root->data) + " -> ";
            return path_to_helper(root->left, key, path);
        } else {
            path += std::to_string(root->data) + " -> ";
            return path_to_helper(root->right, key, path);
        }


    }

    // Print the path to the key, starting with root
    void tree::path_to(int key) {
        // print path to key recursively using root
        if (root == nullptr) {
            std::cout << "";
        } else {
            std::string path;
            path = path_to_helper(root, key, path);
            if (path != "") {
                std::cout << path << std::endl;
            } else {
                std::cout << "";
            }

        }

    }

    unsigned tree::inorder_traversal_for_size(node *root) {
        if (root == nullptr) {
            return 0;
        }
        unsigned sum = 0;
        sum += inorder_traversal_for_size(root->left);
        sum += inorder_traversal_for_size(root->right);
        sum += root->frequency;
        return sum;
    }

    // Number of items in the tree
    unsigned tree::size() {
        return inorder_traversal_for_size(root);
    }

    void tree::inorder_traversal_with_duplicates(node *root) {
        // print the tree in order with duplicates
        if (root == nullptr) {
            return;
        }
        inorder_traversal_with_duplicates(root->left);
        for (int i = 0; i < root->frequency; i++) {
            std::cout << root->data << " ";
        }
        inorder_traversal_with_duplicates(root->right);


    }

    unsigned tree::depth_helper(node *root) {
        // base case: empty tree has a height of 0
        if (root == nullptr) {
            return 0;
        }

        // recur for the left and right subtree and consider maximum depth
        return 1 + std::max(depth_helper(root->left), depth_helper(root->right));

    }

    // Calculate the depth of the tree, longest string of connections
    unsigned tree::depth() {
        if (root == nullptr) return 0;
        else return depth_helper(root) - 1;
    }

    bool tree::in_tree_helper(node *root, int key) {
        if (root == nullptr) {
            return false;
        }
        if (root->data == key) {
            return true;
        }
        return in_tree_helper(root->left, key) || in_tree_helper(root->right, key);
    }

    // Determine whether the given key is in the tree
    bool tree::in_tree(int key) {
        return in_tree_helper(root, key);

    }

    int tree::get_frequency_helper(node *root, int key) {
        if (root == nullptr) {
            return 0;
        } else if (key == root->data) {
            return root->frequency;
        } else if (key < root->data) {
            return get_frequency_helper(root->left, key);
        } else {
            return get_frequency_helper(root->right, key);
        }
    }

    // Return the number of times that value is in the tree
    int tree::get_frequency(int key) {
        // traverse the tree in order using get_frequency_helper and return the result
        return get_frequency_helper(root, key);

    }

    std::string tree::to_string_helper(node *root) {
        // print the tree in order using root along with duplicates printed one after another
        if (root == nullptr) {
            return "";
        }
        std::string str = to_string_helper(root->left);
        if (root->frequency > 1) {
            // print data frequency times
            for (int i = 0; i < root->frequency; i++) {
                str += std::to_string(root->data) + " ";
            }
        } else {
            str += std::to_string(root->data) + " ";
        }
        str += std::to_string(root->data) + " ";
        str += to_string_helper(root->right);
        return str;


    }

    // Return a string of all of the elements in the tree in order with duplicates included
    std::string tree::to_string() {
        // traverse the tree in order using to_string_helper and return the result
        return to_string_helper(root);
    }

    //Use the to string function for the following two functions
    // store the tree in a vector with duplicates included
    void tree::print_helper(node *root, std::vector<int> &vec) {
        if (root == nullptr) {
            return;
        }
        print_helper(root->left, vec);
        for (int i = 0; i < root->frequency; i++) {
            vec.push_back(root->data);
        }
        print_helper(root->right, vec);

    }

    void tree::print() {
        // print the tree in order with duplicates using to_string_helper
        std::vector<int> vec;
        print_helper(root, vec);
        for (int i = 0; i < vec.size(); i++) {
            std::cout << vec[i] << " ";
        }
        std::cout << "\n";
    }

    // Print the tree least to greatest, Include duplicates
    std::ostream &operator<<(std::ostream &stream, tree &RHS) {
        // print the RHS tree inorrder  using inorder_traversal_with_duplicates and return the stream
        RHS.inorder_traversal_with_duplicates(RHS.root);
        return stream;


    }

    // copy_tree helper function
    node *tree::copy_tree(node *root) {
        if (root == nullptr) {
            return nullptr;
        }
        node *new_node = new node(root->data, root->frequency);
        new_node->left = copy_tree(root->left);
        new_node->right = copy_tree(root->right);
        return new_node;
    }


    // Operator= Overload. Allowing for copying of trees
    tree &tree::operator=(const tree &rhs) {
        // copy the rhs tree into this tree  if this tree is empty
        if (root == nullptr) {
            root = copy_tree(rhs.root);
        } else {
            // otherwise, delete the current tree and copy the rhs tree into this tree
            delete_tree(root);
            root = copy_tree(rhs.root);
        }
        return *this;


    }

    /**************************
     * Extra credit functions *
     **************************/

    void tree::values_above_helper(node *root, int key, std::vector<int> &values) {

    }

    // Return a vector with all of the nodes that are greater than the input key in the tree
    std::vector<int> tree::values_above(int key) {

    }

    // Merge rhs into this. Demo to a TA for credit
    tree tree::operator+(const tree &rhs) const {

    }

    // Balance the tree using any published algorithm. Demo to a TA for credit
    void tree::balance() {

    }

    /*********************************************************************
     * Recursion Example                                                 *
     * Print the linked list from greatest to least using recursion      *
     *********************************************************************/

    // Auxiliary functions
    void node_print_gtl(node *top) {
        if (top == nullptr) return;
        node_print_gtl(top->right);
        for (int i = 0; i < top->frequency; i++) std::cout << top->data << " ";
        node_print_gtl(top->left);
    }

    void clear(node *to_clear) {
        if (to_clear == nullptr) return;
        if (to_clear->left != nullptr) clear(to_clear->left);
        if (to_clear->right != nullptr) clear(to_clear->right);
        delete to_clear;
    }

    // Class function
    void tree::print_gtl() {
        node_print_gtl(root);
        std::cout << std::endl;
    }

    void tree::delete_tree(node *pNode) {
        // delete the tree rooted at pNode
        if (pNode == nullptr) return;
        if (pNode->left != nullptr) delete_tree(pNode->left);
        if (pNode->right != nullptr) delete_tree(pNode->right);
        delete pNode;

    }
}