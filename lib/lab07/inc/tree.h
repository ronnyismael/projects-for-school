#ifndef CMPE126S18_LABS_TREE_H
#define CMPE126S18_LABS_TREE_H
#include "node.h"
#include <iostream>
#include <vector>

namespace lab7 {
    class tree {
        node *root;
        int tree_size;
    public:
        tree();
        tree(const tree& copy);     //Recursively
        ~tree();                    //Recursively

        void insert(int value);     //Recursively
        void insert_helper(node *&root, int value);
        bool remove(int key);

        bool in_tree(int key);      //Recursively
        bool in_tree_helper(node *root, int key);

        int get_frequency(int key); //Recursively
        int get_frequency_helper(node *root, int key);

        std::string to_string();    //Recursively
        std::string to_string_helper(node *root);

        int level(int key);         //Recursively
        int level_helper(node *root, int key, int level);

        void path_to(int key);      //Recursively
        std::string path_to_helper(node *root, int key,std::string &path);

        unsigned size();

        unsigned inorder_traversal_for_size(node *root);
        void inorder_traversal_with_duplicates(node *root);

        unsigned depth();           //Recursively
        unsigned depth_helper(node *root);

        void print();               //Recursively
        void print_helper(node *root,std::vector<int> &values);

        tree& operator=(const tree &rhs);   //Recursively
        friend std::ostream& operator<<(std::ostream& stream, tree& RHS); //Recursively

        // Extra credit
        std::vector<int> values_above(int key); //Recursively
        void values_above_helper(node *root, int key, std::vector<int> &values);
        tree operator+(const tree &rhs) const;  //Recursively
        void balance();

        // Example recursion
        void print_gtl();

        node *copy_tree_helper(node *root);

        node *copy_tree(node *root);

        void delete_tree(node *pNode);
    };
}

#endif //CMPE126S18_LABS_TREE_H
