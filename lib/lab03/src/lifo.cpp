#include "lifo.h"

namespace lab3
{
    lifo::lifo() 
    {
    //Reserve 100 spaces in lifo_storage
        lifo_storage = lab2::stringVector(100);
        index = -1;
    }

    lifo::lifo(std::string input_string) 
    {
        lifo_storage = lab2::stringVector(100);
        lifo_storage[0] = input_string;
        index = 0;

    }

    lifo::lifo(const lifo &original) 
    {
        lifo_storage = original.lifo_storage;
        index = original.index;
    }

    lifo::~lifo() {
        delete [] this;
    }

    lifo &lifo::operator=(const lifo &right) 
    {
        //return <#initializer#>;
        unsigned rs = right.index+1;
        lifo_storage = right.lifo_storage;
        index = rs-1
        return *this;
    }

    bool lifo::is_empty()
    {
        //return false;
        if (lifo_storage.empty())
        {
            return true;
        }else
        {
            return false;
        }
    }

    int lifo::size()
    {
        //return 0;
        return lifo_storage.size();
    }

    std::string lifo::top()
    {
        //return std::__cxx11::string();
        if (!is_empy())
        {
            return lifo_storage[index];
        }else
        {
            std::cout<<"Is Empty" << endl;
        }
    }

    void lifo::push(std::string input) 
    {
        index++;
        lifo_storage.append(input);
    }

    void lifo::pop() 
    {
        if(!is_empty)
        {
            index--;
        }else
        {
            std::cout<<"Is Empty" << endl;
        }
    }
}