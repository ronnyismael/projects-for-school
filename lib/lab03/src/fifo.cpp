#include "fifo.h"

namespace lab3{
    fifo::fifo() 
    {
    //Reserve 100 spaces in fifo_storage
        fifo_storage.reserve(100);
        front_index = 0;
        back_index = 0; 
    }

    fifo::fifo(std::string input_string) 
    {
        fifo_storage.append(input_string);
        front_index = 0;
        back_index = 0; 
    }

    fifo::fifo(const fifo &original) 
    {
        fifo_storage = original.fifo_storage;
        front_index = original.front_index;
        back_index = original.back_index;
    }

    fifo::~fifo() 
    {
        delete[] this;
    }

    fifo &fifo::operator=(const fifo &right) 
    {
        //return <#initializer#>;
        fifo_storage = right.fifo_storage;
        front_index = right.front_index;
        back_index  = right.back_index;
        return *this;
    }

    bool fifo::is_empty()
    {
        //return false;
        if (fifo_storage.empty())
        {
            return true;
        }else
        {
            return false;
        }
    }

    int fifo::size()
    {
        //return 0;
        if (back_index == -1 && front == -1)
        {
            return 0;
        }else if (back_index == front_index)
        {
            return 1;
        }else
        {
            return back_index-front_index+1;
        }
    }

    std::string fifo::front()
    {
        //return std::__cxx11::string();
        if (!is_empty())
        {
            return fifo_storage[front_index];
        }else
        {
            std::cout << "Is empty" << endl;
        }
    }
    std::string fifo::rear()
    {
        //return std::__cxx11::string();
        if (!is_empty())
        {
            return fifo_storage[back_index];
        }else
        {
            std::cout << "Is empty" << endl;
        }
    }

    void fifo::enqueue(std::string input) 
    {
        back_index++;
        fifo_storage.append(input);
    }

    void fifo::dequeue() 
    {
        if (is_empty)
        {
            std::cout << "Is empty" << endl;
        }else
        {
            if (front_index == back_index)
            {
                front_index=back_index=-1;
            }else
            {
                front_index++;
            }
        }
    }
}
