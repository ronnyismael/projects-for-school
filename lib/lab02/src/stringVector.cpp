#include "../inc/stringVector.h"
#include<iostream>
namespace lab2
{
    stringVector::stringVector() // default constructor
    {
        data = new std::string[1];
        allocated_length = 0; // allocated lenght is nothing but the capacity that the string vector can withhod. 
                              // this should grow dynamically. in other words it should double everytime the container is full.
        length = 0; // length variable holds the current length of the vector at any point of time.
    }
    stringVector::stringVector(unsigned size) //** This is required for Lab 3; we're fixing the capacity to certain static size.
    {
        data = new std::string[size];
        length = 0;
        allocated_length = 100;
    }

    stringVector::~stringVector()
    {
        delete[] data; // destroy the array. This clears the memory that was created for the array.
    }

    unsigned stringVector::size() const
    {
        return length; // Size of an array is length of array at any point in the program's execution.
    }

    unsigned stringVector::capacity() const
    {
        return allocated_length; // capacity of an array is allocated length of array at any point in the program's execution.
    }

    void stringVector::reserve(unsigned new_size)
    {
        if (allocated_length == 0)
        {
            allocated_length = new_size; // explicitly reserve size (new_size) passed as a parameter.
        }
        else if (allocated_length < new_size) // if the current capacity is less than new capacity
        {
            std::string *temp = new std::string[length]; 
            for (int i = 0; i < length; i++)  temp[i] = data[i]; // copy the current data into a temporary array.  
            data = new std::string[new_size]; // create a new array with the size passed as parameter

            /*

                * copy temporary data into newly created array. 
                * This ensures new array of new capacity os created without loosing the data from the old array.
                * Alternatively, you can also create a Temporary StringVector object and use '=' operator overloading to copy data from temporary array to new array.

            */

            for (int i = 0; i < length; i++) data[i] = temp[i]; 
            allocated_length = new_size;
        }
        else
        {
            /* 
             
             *if the new size is less than the current capacity, we need to chop off our current array till the new size.
             Step1: create a temporary array of size = new_size passed as a parameter.
             Step2: copy the data from the current array to temp array that automatically chops off the extra data as we're restricting the size already.
             step3: create a new array of new_size
             step4: copy data from temp array to new aray.
             step5: change the allocated length and length variables to new_size.

            */
            std::string *temp = new std::string[new_size]; //step 1
            for (int i = 0; i < new_size; i++) temp[i] = data[i]; //step2
            data = new std::string[new_size]; //step 3
            for (int i = 0; i < new_size; i++) data[i] = temp[i]; //step 4
            //step 5
            allocated_length = new_size;
            length = new_size;
        }
    }

    bool stringVector::empty() const
    {
        if (size() == 0) return true;
        else return false;
    }

    void stringVector::append(std::string new_data)
    {
        /*

        * Before appending anything to the array, first check if it's a new array with no elements inserted before or not.
        * If the above condition satisfies, i.e a new array, we need to reserve the capacity of 1 and then push the incoming string data.
        * If the array has already reached it's capacity, the capacity needs to be doubled and all the data needs to be copied to newly created array with doubled size.
        *

        */
        if (allocated_length == 0)
            reserve(1);
        else if (length == allocated_length)
        {
            unsigned new_size = 2 * allocated_length; // doubles the size
            reserve(new_size); // reserve method reserves the double size, copies the old data to newly doubled array.
        }
        data[length] = new_data; // the incoming now can be pushed irrespective of any condition mentioned above.
        length = length + 1;
    }

    void stringVector::swap(unsigned pos1, unsigned pos2)
    {
        if (pos1 > size() || pos2 > size()) // if any of the positions out of range, throow error below
            throw std::out_of_range("Index out of Range");
        else
        {
            // Logic to swap data in pos1 and pos2
            std::string temp = data[pos1];
            data[pos1] = data[pos2];
            data[pos2] = temp;
        }
    }

    stringVector &stringVector::operator=(stringVector const &rhs)
    {
        /*
         '=' operator is simply overloaded and used to asign the data from rhs to lhs.
         * This means we need to make current allocated length to rhs length
         * copy everything from rhs to LHS by using a loop.
         * update current length to rhs length
         * allocated_length/ capacity is updated to rhs length
         *  return *this return the the same object.
         
        */
        unsigned rhsSize = rhs.size();
        //std::cout<<"StringVector.cpp Line 131 -->"<<rhsSize<<std::endl;
        std::string *rhsData = rhs.data;
        reserve(rhsSize);
        for(int i=0;i<rhsSize;i++)
            data[i] = rhsData[i];
        length = rhsSize;
        allocated_length= rhsSize;
        return *this;
    }

    std::string &stringVector::operator[](unsigned position)
    {
        /*
         [] operator is overloaded to replicate the same functionality of [] in regular arrays.
         * if the position being accessed is greater than the length of an array, trhow an error.
         * Else return the data at that position.
        */
        if (position >= size())
            throw std::out_of_range("Index out of Range");
        else
            return data[position];
    }

    void stringVector::sort()
    {
        /* Algorithm to bubble sort the array */
        
        int i;
        int j;
        for (j = 0; j < length; j++)
        {
            for (i = j + 1; i < length; i++)
            {
                if (data[i] < data[j])
                {
                    std::string temp = data[j];
                    data[j] = data[i];
                    data[i] = temp;
                }
            }
        }
    }
}
//Original Code Found Here:
/*
#include <iostream>
#include "../inc/stringVector.h"

namespace lab2 {
    stringVector::stringVector()
    {   // Constructor     
        data = new std::string[allocated_length];      
        length = 0;
        allocated_length = 0;
    }
    stringVector::~stringVector()                          
    {  // Frees up reserved memory                      
       delete[] data;
    }
    unsigned stringVector::size() const                    
    {   // Returns length value                
        return length;
    }
    unsigned stringVector::capacity() const                
    {   // Returns reserved value            
        return allocated_length;
    }
    void stringVector::reserve(unsigned new_size) 
    {   // Allows user to choose the allocation size, if it is small than current array then data should be truncated to fit
        std::string *temp = new std::string[new_size];
        for (int i=0; i<new_size; i++)
        {
            if (i<length)
            {
                temp[i] = data[i];
            }
        }
        delete[] data;
        data = temp;
        allocated_length = new_size;
    }
    bool stringVector::empty() const
    {   // Evaluates length
        if(length==0)
        {
            return true;
        }else
        {
            return false;
        }
    }
    void stringVector::append(std::string new_data) 
    {   // append data to end of array, if capacity is zero set it to 1, otherwise double array capacity if this is over capacity
        if (allocated_length == 0)
        {
            allocated_length = 1;
        }else if (length < allocated_length)
            data[length] = new_data;
        }else
        {
            allocated_length *= 2;
        }
    }
    void stringVector::swap(unsigned pos1, unsigned pos2) 
    {   // swap the string in position1 (pos1) of the array with the string in position2, throw an exception if either position is out of bounds
        std::string temp;
        try
        {
            if(pos1<length || pos2<length)
            {
                std::temp = data[pos1];
                data[pos1] = data[pos2];
                data[pos2] = temp;
            }else
            {
                 if(pos1>length)
                 {
                     throw(pos1);
                 }else
                 {
                     throw(pos2)
                 }
            }
        }
        catch (unsigned pos1, unsigned pos2)
        {
            std::cout << "Position out of bounds";
        }
    }
    stringVector &stringVector::operator=(stringVector const &rhs) 
    {   // Copies RHS to object calling the function (this should be a hard COPY, creating a separate object with same values)
        if (this == &rhs)
        {
            return *this;
        }else
        {
            this->data = new std::string[this->allocated_length];
            this->length = rhs.length;
            this->allocated_length = rhs.allocated_length;
            return *this;
        }
    }
    std::string &stringVector::operator[](unsigned position) 
    {   // return a reference to the string at this position, throw an exception if out of bounds
        if (allocated_length == 0)
        {
            throw(position);
        }else if (position >= allocated_length)
        {
            throw(position);
        }
        return data[position];
    }
    void stringVector::sort() 
    {   // use the bubble sort function discussed in lab to sort the vector like a dictionary (lower letters and less letters first)
        for (int i=0; i<length-1; i++)
        {
            for (int j=0; j<length-1; j++)
            {
                if (data[j+1] < data[j])
                {
                    swap (j+1, j);
                }
            }
        }
    }
}*/