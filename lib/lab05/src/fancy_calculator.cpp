#include "fancy_calculator.h"
#include "stack.h"
#include "queue.h"

namespace lab5 {
    /*****Auxillary Function Prototypes****/
    bool is_number(std::string input_string);
    bool is_operator(std::string input_string);
    int operator_priority(std::string operator_in);
    int calc_binary_operation(int lhs, int rhs, std::string op);
    /****End Prototypes*****/

    void calculator::parse_to_infix(std::string &input_expression) {
        lab1::expressionstream parser(input_expression);
        std::string token = parser.get_next_token();
        while (token != "\0") {
            infix_expression.enqueue(token);
            token = parser.get_next_token();
        }
    }

    void calculator::convert_to_postfix(queue infix_expression_copy) {
        std::string current_token;
        stack op_stack;

        while(!infix_expression_copy.isEmpty()) {//while there are tokens to be read:
            current_token = infix_expression_copy.top();
            infix_expression_copy.dequeue();//read a token.
            if (is_number(current_token)) postfix_expression.enqueue(current_token);//if the token is a number, then push it to the output queue.
            else if (is_operator(current_token)) {//if the token is an operator, then:
                while (!op_stack.isEmpty() && operator_priority(op_stack.top()) >= operator_priority(current_token)
                       &&
                       op_stack.top() != "(") {     //while ((there is an operator at the top of the operator stack with
                    //greater precedence) or (the operator at the top of the operator stack has
                    //equal precedence and
                    //the operator is left associative)) and
                    //(the operator at the top of the stack is not a left bracket):
                    postfix_expression.enqueue(
                            op_stack.top()); //pop operators from the operator stack, onto the output queue.
                    op_stack.pop();
                }
                op_stack.push(current_token);
            }//push the read operator onto the operator stack.
            else if(current_token == "(")//if the token is a left bracket (i.e. "("), then:
                op_stack.push(current_token);//push it onto the operator stack.
            else if (current_token == ")") {//if the token is a right bracket (i.e. ")"), then:
                while (op_stack.top() !=
                       "(") { //while the operator at the top of the operator stack is not a left bracket:
                    postfix_expression.enqueue(op_stack.top());
                    op_stack.pop(); //pop operators from the operator stack onto the output queue.
                }
                op_stack.pop();//pop the left bracket from the stack.
            }
        }
        while(!op_stack.isEmpty()) {    //while there are still operator tokens on the stack:
            if (op_stack.top() ==
                "(") { throw "mismatched parenthesis"; }///* if the operator token on the top of the stack is a bracket, then
            //there are mismatched parentheses. */
            postfix_expression.enqueue(op_stack.top());//pop the operator onto the output queue.
            op_stack.pop();
        }
        //exit.
    }

    calculator::calculator() = default;

    calculator::calculator(std::string &input_expression) {
        parse_to_infix(input_expression);
        convert_to_postfix(infix_expression);
    }

    std::istream &operator>>(std::istream &stream, calculator &RHS) {
        std::istreambuf_iterator<char> eos;
        std::string input_expression(std::istreambuf_iterator<char>(stream), eos);

        RHS.clear_expressions();

        RHS.parse_to_infix(input_expression);
        RHS.convert_to_postfix(RHS.infix_expression);
        return stream;
    }

    int calculator::calculate() {
        stack op_stack;
        queue eq_queue(postfix_expression);
        int lhs,rhs;

        while(!eq_queue.isEmpty()){
            while(is_number(eq_queue.top())){
                op_stack.push(eq_queue.top());
                eq_queue.dequeue();
            }
            rhs = stoi(op_stack.top());
            op_stack.pop();
            lhs = stoi(op_stack.top());
            op_stack.pop();
            op_stack.push(std::to_string(calc_binary_operation(lhs,rhs,eq_queue.top())));
            eq_queue.dequeue();
        }
        return stoi(op_stack.top());
    }

    std::ostream &operator<<(std::ostream &stream, calculator &RHS) {
        //"Infix: #,#,#,#\nPostfix: #,#,#,#"
        queue expression_copy(RHS.infix_expression);
        stream << "Infix: " << expression_copy.top();
        for(expression_copy.dequeue(); !expression_copy.isEmpty(); expression_copy.dequeue()) stream << "," << expression_copy.top();

        expression_copy = RHS.postfix_expression;
        stream << "\nPostfix: " << expression_copy.top();
        for(expression_copy.dequeue(); !expression_copy.isEmpty(); expression_copy.dequeue()) stream << "," << expression_copy.top();

        return stream;
    }

    void calculator::clear_expressions() {
        while(!infix_expression.isEmpty()) infix_expression.dequeue();
        while(!postfix_expression.isEmpty()) postfix_expression.dequeue();
    }

    // AUXILIARY FUNCTIONS DEFINITIONS
    bool is_number(std::string input_string){
        std::string::iterator location = input_string.begin();
        if(*location >= '0' && *location <= '9') return true;
        if(*location != '-')return false;
        return(*++location >='0' && *location <= '9');
    }

    bool is_operator(std::string input_string){
        std::string::iterator location = input_string.begin();
        if(*location == '+' || *location == '*' || *location == '/') return true;
        return (*location == '-' && (location+1) == input_string.end());
    }

    int operator_priority(std::string operator_in){
        std::string::iterator location = operator_in.begin();
        switch (*location){
            case '^': return 4;
            case '*': return 3;
            case '/': return 3;
            case '+': return 2;
            case '-': return 2;
            default: return -1;
        }
    }

    int calc_binary_operation(int lhs, int rhs, std::string op){
        std::string::iterator op_iter = op.begin();
        switch (*op_iter){
            case '^': return lhs ^ rhs;
            case '*': return lhs * rhs;
            case '/': return lhs / rhs;
            case '+': return lhs + rhs;
            case '-': return lhs - rhs;
            default: throw "unknown operator";
        }
    }
}
