# Final Exam: Design a Student Management System #
For this lab, you will be implementing a Binary Search Tree to store student details like roll number, name, points and do various operations on the tree.

The construction of tree should happen based on two conditions:
1. if the criteria is  'r' which stands for roll number, the insertion should be done in the order of roll number.
2. if the criteria is 'p' which stands for points, the insertion should be done in the order of points.

Below is the stricture of the tree node

        unsigned roll_no;
        std::string name;
        unsigned points;
        node *left;
        node *right;

The tree class will have the following attributes,

    node *root
    int tree_size ; to track the size of the tree
    char criteria ; based on which criteria the tree is constructed

### Lab Instructions ###
You will be writing all of the code for the implementation of a tree. You can use any helper functions you would like, but all of the functions that are provided to you need to do the functions that are commented above them. 

You can use either iterative or recursive approach to solve the problem.


student_tree(char criteria)

student_tree(const student_tree& copy)

~student_tree();    

void insert(unsigned rollno, std::string name, unsigned points);  
###### for insert function, the data passed should have all three parameters. But the insertion order should be based on the criteria  provided while initializing the tree class.
bool remove(unsigned rollno);
###### for remove function, assume that only roll numbers are passed. Removal of nodes based on points is not considered to reduce the complexity of the problem.
bool in_tree(unsigned key);

int getHighestPoints(); 

int getLowestPoints(); 

std::vector<int> getPointsinAscending();

std::vector<int> getPointsinDescending();

std::vector<int> getPointsAbove( int threshold);

std::vector<int> getPointsBelow( int threshold); 



### Hints ###
- `left` children of a parent are **ALWAYS** less than the parent 
- `right` children of a a parent are **ALWAYS** greater than the parent
- You cannot access the parent from a child. It is a one way connection.
- you can write any number of auxilary functions.
- Wikipedia article on [Binary Search Tree](https://en.wikipedia.org/wiki/Binary_search_tree)

### Important Instructions ###
- You must complete solve the problem within the time limit.
- you have to submit your test cases execution screenshot by the end of the exam. It MUST match the test case execution while grading.Otherwise 0 points will be awarded.
- This is an open book exam which means you can use your class material, lecture slides, textbooks or refer the internet. However, copy and pasting the entire source code from the internet is considered as plagiarism.
- The original code has lot of auxilary functions. You can use skip of the auxilary functions if you are using iterative approach or if you feel those functions are not necessary. However, we recommend to use iterative approach.
