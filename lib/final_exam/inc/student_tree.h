#ifndef CMPE126S18_LABS_STUDENT_TREE_H
#define CMPE126S18_LABS_STUDENT_TREE_H
#include "node.h"
#include <iostream>
#include <vector>

namespace final_exam {
    class student_tree {
        node *root;
        int tree_size;
        char criteria;
    public:
        student_tree(char criteria);
        student_tree(const student_tree& copy);     //Recursively
        ~student_tree();                    //Recursively

        void insert(unsigned rollno, std::string name, unsigned points);     //Recursively
        void insert_helper(node *&root, unsigned rollno, std::string name, unsigned points);
        bool remove(unsigned rollno);
        bool remove_helper(node *&root, unsigned rollno);

        bool in_tree(unsigned key);      //Recursively
        bool in_tree_helper(node *root, unsigned key);
        int getHighestPoints(); // return highest points in the tree
        int getLowestPoints(); // return lowest points in the tree

        std::vector<int> getPointsinAscending(); // returns a vector of all the points in the student_tree in ascending order
        void getPointsinAscending_helper(node *pNode, std::vector<int> &vec);

        std::vector<int> getPointsinDescending(); // returns a vector of all the points in the student_tree in descending order
        void getPointsinDescending_helper(node *pNode, std::vector<int> &vec);
        std::vector<int> getPointsAbove( int threshold); // returns a vector of all the points in the student_tree above the threshold
        void getPointsAbove_helper(node *root, int threshold, std::vector<int> &vec);
        std::vector<int> getPointsBelow( int threshold); // returns a vector of all the points in the student_tree below the threshold
        void getPointsBelow_helper(node *pNode, int threshold, std::vector<int> &vec);



        unsigned size();
        unsigned inorder_traversal_for_size(node *root);

        void print();               //Recursively
        void print_helper(node *root,std::vector<std::string> &values);

        student_tree& operator=(node *rhs);   //Recursively
        friend std::ostream& operator<<(std::ostream& stream, student_tree& RHS); //Recursively


        void inorder_traversal(node *pNode, student_tree tree1);

        node *copy(node *pNode);




    };
}

#endif //CMPE126S18_LABS_STUDENT_TREE_H
