#ifndef CMPE126S18_LABS_LIB_LAB7_NODE_H
#define CMPE126S18_LABS_LIB_LAB7_NODE_H
#include <string>
namespace final_exam {
    class node{
    public:
        // create a node with data to store student information rollno, name, and grade
        node(int rollno, std::string name, int points){
            this->rollno = rollno;
            this->name = name;
            this->points = points;
            left = nullptr;
            right = nullptr;
        }



        std::string name;  // Name of the student
        unsigned rollno;        // Roll number of the student
        unsigned points;         // Grade of the student
        node *left;       // Pointer to the left child
        node* right;      // Pointer to the right child
        explicit node(node* copy_node){
            name = copy_node->name;
            rollno = copy_node->rollno;
            points = copy_node->points;
            left = nullptr;
            right = nullptr;
        }
    };
}
#endif //CMPE126S18_LABS_NODE_H