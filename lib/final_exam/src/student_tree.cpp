#include "../inc/student_tree.h"
#include <iostream>
#include <algorithm>
#include <cmath>

namespace final_exam {
    void clear(node *to_clear){
        if (to_clear->left) {
            clear(to_clear->left);
        }
        if (to_clear->right) {
            clear(to_clear->right);
        }
        delete to_clear;
    }

    // Construct an empty student_tree
    student_tree::student_tree(char criteria) {
        root = nullptr;
        tree_size = 0;
        this->criteria = criteria;
    }

    // Copy constructor
    student_tree::student_tree(const student_tree &copy) {

    }

    // Deconstruct student_tree
    student_tree::~student_tree() {
        clear(root);
    }

    void student_tree::insert_helper(node *&root, unsigned rollno, std::string name, unsigned points) {
        if(criteria == 'r'){
            if(root == nullptr){
                root = new node(rollno, name, points);
                tree_size++;
            }else if(rollno < root->rollno){
                insert_helper(root->left, rollno, name, points);
            }else if(rollno > root->rollno){
                insert_helper(root->right, rollno, name, points);
            }else
                return 0;
        }else if(criteria == 'p'){
            if(root == nullptr){
                root = new node(rollno, name, points);
                tree_size++;
            }else if(points < root->points){
                insert_helper(root->left, rollno, name, points);
            }else if(points > root->points){
                insert_helper(root->right, rollno, name, points)
            }else
                return 0;
        }
    }

    // Insert
    void student_tree::insert(unsigned rollno, std::string name, unsigned points) {
        // insert node into tree based on the criteria.
        if(criteria == 'r'){
            insert_helper(root, rollno, name, points);
        }else if (criteria == 'p'){
            insert_helper(root, rollno, name, points);
        }
    }

    bool student_tree::remove(unsigned rollno) {
        // remove the node with the given rollno from the tree. no need to consider points for removing.
     if (root == nullptr) {

            return false;

        }

        // if root is key, remove root

        if (root->rollno == rollno) {

            node *temp = root;

            root = root->right;

            delete temp;

            tree_size--;

            return true;

        }

        node *current = root;

        node *parent = nullptr;

        while (current != nullptr) {

            if (current->rollno == rollno) {

                  if (current->left == nullptr) {

                        if (parent->left == current) {

                            parent->left = current->right;

                        } else {

                            parent->right = current->right;

                        }

                        delete current;

                        tree_size--;

                        return true;

                    } else if (current->right == nullptr) {

                        if (parent->left == current) {

                            parent->left = current->left;

                        } else {

                            parent->right = current->left;

                        }

                        delete current;

                        tree_size--;

                        return true;

                    } else {

                        node *temp = current->right;

                        while (temp->left != nullptr) {

                            temp = temp->left;

                        }

                        temp->left = current->left;

                        if (parent->left == current) {

                            parent->left = current->right;

                        } else {

                            parent->right = current->right;

                        }

                        delete current;

                        tree_size--;

                        return true;

                    }

            }

            else if (current->rollno > rollno) {

                parent = current;

                current = current->left;

            } else {

                parent = current;

                current = current->right;

            }

        }
    }

    unsigned student_tree::inorder_traversal_for_size(node *root) {
        // inorder traversal for size
        if (root == nullptr) {
            return 0;
        }
        unsigned sum = 0;
        sum += inorder_traversal_for_size(root->left);
        sum += inorder_traversal_for_size(root->right);
        return sum;
    }

    // Number of items in the student_tree
    unsigned student_tree::size() {
        // return the number of items in the student_tree or in other words the number of nodes in the tree.
        return inorder_traversal_for_size(root);
    }

    bool student_tree::in_tree_helper(node *root, unsigned key) {
       if(criteria == 'r'){
            if(root == nullptr){
                return false;
            }
            if(root->rollno == key){
                return true;
            }
            return in_tree_helper(root->left, key) || in_tree_helper(root->right, key);
       }else if(criteria == 'p'){
            if(root == nullptr){
                return false;
            }
            if(root->points == key){
                return true;
            }
            return in_tree_helper(root->left, key) || in_tree_helper(root->right, key);
       }
    }

    // Determine whether the given key is in the student_tree
    bool student_tree::in_tree(unsigned key) {
        return in_tree_helper(root, key);
    }

    //Use the to string function for the following two functions
    // store the student_tree in a vector with duplicates included
    void student_tree::print_helper(node *root, std::vector<std::string> &vec) {
        if(root == nullptr){
            return;
        }
        print_helper(root->left, vec);
        print_helper(root->right,vec);
    }

    void student_tree::print() {
        // print the student_tree in order with duplicates using to_string_helper
        // print format : {rollno, name, points} seperated by space
        // eg : {1, "John", 100} {2, "Jane", 200} {3, "Rob", 300}
        std::vector<std::string> vec;
        print_helper(root, vec);
        for (int i=0; i<vec.size(); i++){
            std::cout << "{" << vec[i] << "} ";
        }
        std::cout << "\n";
    }


    int student_tree::getHighestPoints() {
        // get the highest marks iteratively
        node* tmp = root;
        while(tmp->right != nullptr){
            tmp = tmp->right;
        }
        return tmp->points;
    }

    int student_tree::getLowestPoints() {
        // get the lowest marks
        node* tmp = root;
        while(tmp->left != nullptr){
            tmp = tmp->left;
        }
        return tmp->points;
    }

    std::vector<int> student_tree::getPointsAbove( int threshold) {
        // get the students with points above the threshold
        std::vector<int> vec;
        getPointsAbove_helper(root, threshold, vec);
        return vec;
    }
    void student_tree::getPointsAbove_helper(node *pNode, int threshold, std::vector<int> &vec) {
        if(pNode = nullptr){
            return 0;
        }
        getPointsAbove_helper(pNode->left, threshold, vec);
        if(pNode->points > threshold){
            vec.push_back(pNode->points);
        }
        getPointsAbove_helper(pNode->right, threshold, vec);
    }

    std::vector<int> student_tree::getPointsBelow( int threshold) {
        std::vector<int> vec;
        getPointsBelow_helper(root, threshold, vec);
        return vec;
    }
    void student_tree::getPointsBelow_helper(node *pNode, int threshold, std::vector<int> &vec) {
        if(pNode = nullptr){
            return 0;
        }
        getPointsBelow_helper(pNode->left, threshold, vec);
        if(pNode->points < threshold){
            vec.push_back(pNode->points);
        }
        getPointsBelow_helper(pNode->right, threshold, vec);
    }



    std::vector<int> student_tree::getPointsinAscending() {
        std::vector<int> vec;
        getPointsinAscending_helper(root, vec);
        return vec;
    }

    void student_tree::getPointsinAscending_helper(node *pNode, std::vector<int> &vec) {
        if(pNode == nullptr){
            return 0;
        }
        getPointsinAscending_helper(pNode->left, vec);
        vec.push_back(pNode->points);
        getPointsinAscending_helper(pNode->right, vec);
    }

    std::vector<int> student_tree::getPointsinDescending() {
        // get the students with points in descending order
        std::vector<int> vec;
        getPointsinDecending_helper(root, vec);
        return vec;
    }

    void student_tree::getPointsinDescending_helper(node *pNode, std::vector<int> &vec) {
        if(pNode == nullptr){
            return 0;
        }
        getPointsinAscending_helper(pNode->right, vec);
        vec.push_back(pNode->points);
        getPointsinAscending_helper(pNode->left, vec);
    }

    // auxilay functions
    void student_tree::inorder_traversal(node *pNode, student_tree tree1) {
        if(root == nullptr){
            return 0;
        }
        inorder_traversal(root->left, tree1);
        inorder_traversal(root->right, tree1);
    }

    node *student_tree::copy(node *pNode) {}
}