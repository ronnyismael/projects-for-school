#include "../inc/doubly_linked_list.h"

namespace lab6{
    doubly_linked_list::doubly_linked_list() {
        head = nullptr;
        tail = nullptr;
    }

    doubly_linked_list::doubly_linked_list(int input) {
        head = tail = new node(input);
    }

    doubly_linked_list::doubly_linked_list(std::vector<int> vector_input) {

    }

    doubly_linked_list::doubly_linked_list(const doubly_linked_list &original) {
        head=tail= nullptr;
        if(original.is_empty()){return;}

        node * node_jumper = original.head;

        while(node_jumper){
            append(node_jumper->data);
            node_jumper = node_jumper->next;
        }
    }

    doubly_linked_list::~doubly_linked_list() {

    }

    bool doubly_linked_list::is_valid_location(unsigned location) const{
        return location < size();
    }
    node *doubly_linked_list::find_node(unsigned int location) const{
        if(!is_valid_location(location))throw std::invalid_argument( "received location outside of bounds" );
        node* node_jumper = head;
        for(unsigned i=0; i<location; ++i) node_jumper = node_jumper->next;

        return node_jumper;
    }

    int doubly_linked_list::get_data(unsigned position) {
        int data = find_node(position)->data;
        return data;

    }

    std::vector<int> doubly_linked_list::get_set(unsigned position_from, unsigned position_to) {
        std::vector<int> set;
        if(!is_valid_location(position_from) || !is_valid_location(position_to)) return set;
        else{
            // travese linked list from given position to given position and add to vector
            node* node_jumper = find_node(position_from);
            for(unsigned i=position_from; i<=position_to; ++i){
                set.push_back(node_jumper->data);
                node_jumper = node_jumper->next;
            }
            return set;
        }
    }

    unsigned doubly_linked_list::size() const {
        node * node_jumper=head;
        unsigned size=0;

        while(node_jumper){
            node_jumper=node_jumper->next;
            ++size;
        }
        return size;

    }

    bool doubly_linked_list::is_empty() const {
        if((head && !tail) || (!head && tail))throw std::logic_error ("Error head and tail are not both null"); // this should never happen internal error
        return !head;

    }

    void doubly_linked_list::append(int input) {
        if(is_empty()) {head=tail=new node(input); return;}
        auto newNode = new node(input);
        tail->next= newNode;
        newNode->prev = tail;
        tail = newNode;
    }

    void doubly_linked_list::insert(int input, unsigned int location) {
        if(!is_valid_location(location) && (is_empty() && location != 0))throw std::invalid_argument( "received location outside of bounds" );
        //todo: check for valid location before inserting
        if(location == 0 && is_empty()) head = tail = new node(input);
        else
        {
            if (location == 0){
                auto * temp = new node(input);
                temp->next=head;
                temp->prev = nullptr;
                head=temp;
            }
            else if (location == size()){
                append(input);
            }
            else{
                node * node_holder = find_node(location-1);
                auto * node_creater = new node(input);
                node_creater->next = node_holder->next;
                node_holder->next = node_creater;
            }
        }
            
    }

    void doubly_linked_list::remove(unsigned location) {
        if (!is_valid_location(location))throw std::invalid_argument( "received location outside of bounds" );
        if(head == tail){
            delete head;
            head = tail = nullptr;
        }
        else if(location == 0){
            node * node_to_delete = head;
            head = head->next;
            delete node_to_delete;
        }else{
           // delete a node at location in a doubly linked list
            node * node_to_delete = find_node(location);
            node * node_holder = node_to_delete->prev;
            node_holder->next = node_to_delete->next;
            delete node_to_delete;

        }
    }

    doubly_linked_list doubly_linked_list::split(unsigned position) {
        // split a doubly linked list into two at given position and return second half of list
        if(!is_valid_location(position))throw std::invalid_argument( "received location outside of bounds" );
        if(position == 0) return *this;
        if(position == size()) throw std::invalid_argument( "received position outside of bounds" );
        if(position == 1) return *this;
        if(position == size()-1) return *this;
        doubly_linked_list second_half;
        node * node_jumper = find_node(position-1);
        second_half.head = node_jumper->next;
        second_half.tail = tail;
        node_jumper->next = nullptr;
        tail = node_jumper;
        return second_half;


    }

    doubly_linked_list doubly_linked_list::split_set(unsigned position_1, unsigned position_2) {
        // not required to implement

    }

    void doubly_linked_list::swap(unsigned position_1, unsigned position_2) {
        // swap two nodes at given positions
        if(!is_valid_location(position_1) || !is_valid_location(position_2))throw std::invalid_argument( "received location outside of bounds" );
        if(position_1 == position_2) return;
        node * node_holder_1 = find_node(position_1);
        node * node_holder_2 = find_node(position_2);
        int temp = node_holder_1->data;
        node_holder_1->data = node_holder_2->data;
        node_holder_2->data = temp;

    }

    void doubly_linked_list::swap_set(unsigned location_1_start, unsigned location_1_end, unsigned location_2_start,
                                      unsigned location_2_end) {
        while(location_1_start<=location_1_end){
            swap(location_1_start,location_2_start);
            location_1_start++;
            location_2_start++;
        }

    }



    void doubly_linked_list::sort() {
        // Implement Insertion Sort
    }

    doubly_linked_list doubly_linked_list::operator+(const doubly_linked_list &rhs) {
        // create a deep copy of the rhs list and add it to the end of this list
        doubly_linked_list temp;
        temp.head = head;
        temp.tail = tail;
        temp.append(rhs.head->data);
        node * node_jumper = rhs.head->next;
        while(node_jumper){
            temp.append(node_jumper->data);
            node_jumper = node_jumper->next;
        }
        return temp;

        

    }

    doubly_linked_list& doubly_linked_list::operator=(const doubly_linked_list &rhs) {
        if(this == &rhs)return *this;

        if(rhs.is_empty()){head=tail= nullptr; return *this;}

        node * node_jumper = rhs.head;

        while(node_jumper){
            append(node_jumper->data);
            node_jumper = node_jumper->next;
        }

        return *this;

    }

    doubly_linked_list& doubly_linked_list::operator+=(const doubly_linked_list &rhs) {
        // create a deep copy of the rhs list and add it to the end of this list
        doubly_linked_list temp;
        temp.head = head;
        temp.tail = tail;
        temp.append(rhs.head->data);
        node * node_jumper = rhs.head->next;
        while(node_jumper){
            temp.append(node_jumper->data);
            node_jumper = node_jumper->next;
        }
        return temp;

    }

    bool doubly_linked_list::operator==(const doubly_linked_list &rhs) {
        // compare two doubly linked lists and return true if they are equal
        if(rhs.is_empty() && is_empty()) return true;
        if(rhs.is_empty() || is_empty()) return false;
        if(rhs.size() != size()) return false;
        node * rhs_node_jumper = rhs.head;
        node * this_node_jumper = head;
        while(rhs_node_jumper){
            if(rhs_node_jumper->data != this_node_jumper->data) return false;
            rhs_node_jumper = rhs_node_jumper->next;
            this_node_jumper = this_node_jumper->next;
        }
        return true;


    }

    std::ostream &operator<<(std::ostream &stream, doubly_linked_list &RHS) {
        // print the doubly linked list to the given output stream and make last node -> nullptr and return stream
        if(RHS.is_empty()){
            stream << "nullptr";
            return stream;
        }
        node * node_jumper = RHS.head;
        while(node_jumper){
            stream << node_jumper->data << " <-> ";
            node_jumper = node_jumper->next;
        }
        //stream << "nullptr";
        return stream;




    }

    std::istream &operator>>(std::istream &stream, doubly_linked_list &RHS) {
        // overload >> operator to read in a doubly linked list
        int input;
        while(stream >> input){
            RHS.append(input);
        }
        return stream;


    }
}

