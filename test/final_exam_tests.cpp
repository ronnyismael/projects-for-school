#include "gtest/gtest.h"
#include "../lib/final_exam//inc/student_tree.h"
#include <algorithm>
class finalExamFixture : public ::testing::Test {
protected:
    virtual void SetUp() {
        test_points=std::vector<unsigned>({12,8,14,15,18,16,17,19,22,20,4,10,9,13,11,5,7,6,2,1,3});
        roll_no=std::vector<unsigned>({3,1,2,6,7,5,11,13,9,10,4,20,22,19,17,16,18,15,14,8,12});
        BST_by_roll_no = new final_exam::student_tree('r');
        BST_by_points = new final_exam::student_tree('p');
        for(int i=0;i<test_points.size();i++){
            BST_by_roll_no->insert(roll_no[i],"student"+std::to_string(i),test_points[i]);
            BST_by_points->insert(roll_no[i],"student"+std::to_string(i),test_points[i]);
        }

    }

public:
    final_exam::student_tree *BST_by_roll_no, *BST_by_points;
    std::vector <unsigned> test_points;
    std::vector <unsigned> roll_no;

};
TEST_F(finalExamFixture, construct_with_check_size) {
    EXPECT_EQ(BST_by_roll_no->size(), 21);
    EXPECT_EQ(BST_by_points->size(), 21);
}
TEST_F(finalExamFixture, crash_test) {
    final_exam::student_tree *BST_by_roll_no = new final_exam::student_tree('r');
    final_exam::student_tree *BST_by_points = new final_exam::student_tree('p');
    BST_by_roll_no->insert(22,"student22",7);
    BST_by_points->insert(22,"student22",7);
    EXPECT_EQ(BST_by_roll_no->size(), 1);
    EXPECT_EQ(BST_by_points->size(), 1);
    BST_by_roll_no->insert(23,"student23",8);
    BST_by_points->insert(23,"student23",8);
    BST_by_roll_no->insert(21,"student23",6);
    BST_by_points->insert(21,"student23",6);
    BST_by_roll_no->insert(20,"student23",5);
    BST_by_points->insert(20,"student23",5);
    BST_by_roll_no->insert(24,"student23",9);
    BST_by_points->insert(24,"student23",9);
    EXPECT_TRUE(BST_by_roll_no->in_tree(23));
    EXPECT_TRUE(BST_by_points->in_tree(8));
    EXPECT_TRUE(BST_by_roll_no->remove(23));
    EXPECT_TRUE(BST_by_points->remove(23));
    EXPECT_EQ(BST_by_points->getHighestPoints(), 9);
    EXPECT_EQ(BST_by_points->getLowestPoints(), 5);
}

TEST_F(finalExamFixture, size)
{
    EXPECT_EQ(BST_by_roll_no->size(), 21);
    BST_by_roll_no->insert(23,"student23",27);
    EXPECT_EQ(BST_by_roll_no->size(), 22);
    BST_by_points->insert(23,"student23",27);
    EXPECT_EQ(BST_by_points->size(), 22);
}

TEST_F(finalExamFixture, in_tree)
{
    BST_by_roll_no->print();

    EXPECT_EQ(BST_by_roll_no->in_tree(23), false);
    EXPECT_EQ(BST_by_roll_no->in_tree(4), true);
    EXPECT_EQ(BST_by_points->in_tree(15), true);
    EXPECT_EQ(BST_by_points->in_tree(21), false);
}

TEST_F(finalExamFixture, print)
{
    testing::internal::CaptureStdout();
    BST_by_roll_no->print();
    std::string output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, std::string("{1|student1|8} {2|student2|14} {3|student0|12} {4|student10|4} {5|student5|16} {6|student3|15} {7|student4|18} {8|student19|1} {9|student8|22} {10|student9|20} {11|student6|17} {12|student20|3} {13|student7|19} {14|student18|2} {15|student17|6} {16|student15|5} {17|student14|11} {18|student16|7} {19|student13|13} {20|student11|10} {22|student12|9} \n"));
}

TEST_F(finalExamFixture, remove_leaf)
{
    EXPECT_EQ(BST_by_roll_no->remove(12), true);
    EXPECT_EQ(BST_by_roll_no->size(), 20);
}

TEST_F(finalExamFixture, remove_parent)
{
    //BST_by_roll_no->print();
    EXPECT_EQ(BST_by_roll_no->remove(12), true);
    EXPECT_EQ(BST_by_roll_no->size(), 20);
}

TEST_F(finalExamFixture, remove_root)
{
    EXPECT_EQ(BST_by_roll_no->remove(1), true);
    EXPECT_EQ(BST_by_roll_no->size(), 20);
}

TEST_F(finalExamFixture, get_highest_points)
{
    EXPECT_EQ(BST_by_points->getHighestPoints(), 22);
    BST_by_points->insert(23,"student23",27);
    EXPECT_EQ(BST_by_points->getHighestPoints(), 27);
}

TEST_F(finalExamFixture, get_lowest_points){
    EXPECT_EQ(BST_by_points->getLowestPoints(), 1);
}

TEST_F(finalExamFixture, get_points_ascending){
    std::vector<int> result = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22};
    std::vector<int> points_ascending;
    points_ascending = BST_by_points->getPointsinAscending();
    for(int i = 0; i < points_ascending.size(); i++){
        EXPECT_EQ(points_ascending[i], result[i]);
    }
}
TEST_F(finalExamFixture, get_points_descending){
    std::vector<int> result = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22};
    std::reverse(result.begin(), result.end());
    std::vector<int> points_descending;
    points_descending = BST_by_points->getPointsinDescending();
    for(int i = 0; i < points_descending.size(); i++){
        EXPECT_EQ(points_descending[i], result[i]);
    }
}

TEST_F(finalExamFixture, get_points_above){
    std::vector<int> result = {16,17,18,19,20,22};
    std::vector<int> points_above;
    points_above = BST_by_points->getPointsAbove(15);
    for(int i = 0; i < points_above.size(); i++){
        EXPECT_EQ(points_above[i], result[i]);
    }
}

TEST_F(finalExamFixture, get_points_below){
    std::vector<int> result = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
    std::vector<int> points_below;
    points_below = BST_by_points->getPointsBelow(15);
    for(int i = 0; i < points_below.size(); i++){
        EXPECT_EQ(points_below[i], result[i]);
    }
}





